#! /bin/sh

#
# Copyright Rainer Wichmann (2006)
#
# License Information:
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

MAXTEST=4; export MAXTEST
LOGFILE="$PW_DIR/.samhain_log"; export LOGFILE
RCFILE="$PW_DIR/testrc_1.dyn";  export RCFILE

testrun1b_modrc ()
{
        ORIGINAL="\[EOF\]"
        REPLACEMENT="\[PortCheck\]"
        ex -s $RCFILE <<EOF
%s/$ORIGINAL/$REPLACEMENT/g
wq
EOF

        echo "PortCheckActive = yes" >>"$RCFILE"
        echo "PortCheckInterface = 127.0.0.1" >>"$RCFILE"
}

testrun1b_internal ()
{
	BUILDOPTS="$1"
	#
	# test standalone compilation
	#
	[ -z "$verbose" ] || { echo; echo "${S}Building standalone agent${E}"; echo; }
	#
	if test -r "Makefile"; then
		$MAKE distclean >/dev/null >&1
	fi
	#
	# Bootstrapping
	#
	${TOP_SRCDIR}/configure >/dev/null 2>/dev/null
	if test x$? = x0; then
		[ -z "$verbose" ] ||     log_msg_ok "configure (bootstrap)..."; 
		$MAKE  > /dev/null 2>&1
		if test x$? = x0; then
		    [ -z "$verbose" ] || log_msg_ok "make (bootstrap)..."; 
		else
		    [ -z "$quiet" ] &&   log_msg_fail "make (bootstrap)..."; 
		    return 1
		fi

	else
		[ -z "$quiet" ] &&       log_msg_fail "configure (bootstrap)...";
		return 1
	fi
	#
	#
	${TOP_SRCDIR}/configure ${BUILDOPTS} 2>/dev/null
	#
	#
	if test x$? = x0; then
		[ -z "$verbose" ] ||     log_msg_ok "configure..."; 
		$MAKE  > /dev/null 2>&1
		if test x$? = x0; then
		    [ -z "$verbose" ] || log_msg_ok "make..."; 
		else
		    [ -z "$quiet" ] &&   log_msg_fail "make..."; 
		    return 1
		fi

	else
		[ -z "$quiet" ] &&       log_msg_fail "configure...";
		return 1
	fi

	cp ${SCRIPTDIR}/testrc_1i.dyn "$RCFILE"
	

	if test x$? = x0; then
	    [ -z "$verbose" ] || log_msg_ok    "copy signed config file...";
	else
	    [ -z "$quiet" ]   && log_msg_fail  "copy signed config file...";
	    return 1
	fi

	if test "x$2" = "x"; then
	    :
	else
	    CONVERT="$2"
	    if test -f "${TOP_SRCDIR}/stealth_template.jpg"; then
		[ -z "$verbose" ] || log_msg_ok "convert..."
		"${CONVERT}" +compress "${TOP_SRCDIR}/stealth_template.jpg" stealth_template.ps >/dev/null
	    else
		[ -z "$quiet" ]   && log_msg_fail  "cannot find file stealth_template.jpg"
		return 1
	    fi
	    if [ $? -ne 0 ]; then
		[ -z "$quiet" ]   && log_msg_fail  "${CONVERT} +compress ${TOP_SRCDIR}/stealth_template.jpg stealth_template.ps";
		return 1
	    fi

	    [ -z "$verbose" ] || log_msg_ok "hide..."
	    ./samhain_stealth -s stealth_template.ps "$RCFILE" >/dev/null
	    if [ $? -ne 0 ]; then
		[ -z "$quiet" ]   && log_msg_fail  "${CONVERT} +compress ${TOP_SRCDIR}/stealth_template.jpg stealth_template.ps";
		return 1
	    fi

	    mv -f stealth_template.ps "$RCFILE"
	    if [ $? -ne 0 ]; then
		[ -z "$quiet" ]   && log_msg_fail  "mv -f stealth_template.ps $RCFILE";
		return 1
	    fi

	fi

	rm -f ./.samhain_file
	rm -f ./.samhain_log
	rm -f ./.samhain_lock

	./samhain -t init -p none -l info

	if test x$? = x0; then
	    [ -z "$verbose" ] || log_msg_ok    "init...";
	else
	    [ -z "$quiet" ]   && log_msg_fail  "init...";
	    return 1
	fi

	cp ${SCRIPTDIR}/test1i_file.sig $PW_DIR/.samhain_file
	if test x$? = x0; then
	    [ -z "$verbose" ] || log_msg_ok    "copy signed database file...";
	else
	    [ -z "$quiet" ]   && log_msg_fail  "copy signed database file...";
	    return 1
	fi
}

testrun1b_nogpg ()
{
	BUILDOPTS="$1"
	#
	# test standalone compilation
	#
	[ -z "$verbose" ] || { echo; echo "${S}Building standalone agent${E}"; echo; }
	#
	if test -r "Makefile"; then
		$MAKE distclean >/dev/null >&1
	fi

	${TOP_SRCDIR}/configure ${BUILDOPTS} 2>/dev/null 
        #
	#
	if test x$? = x0; then
		[ -z "$verbose" ] ||     log_msg_ok "configure..."; 
		$MAKE  > /dev/null 2>&1
		if test x$? = x0; then
		    [ -z "$verbose" ] || log_msg_ok "make..."; 
		else
		    [ -z "$quiet" ] &&   log_msg_fail "make..."; 
		    return 1
		fi

	else
		[ -z "$quiet" ] &&       log_msg_fail "configure...";
		return 1
	fi

	rm -f ./.samhain_file
	rm -f ./.samhain_log
	rm -f ./.samhain_lock

	cp "${SCRIPTDIR}/testrc_1" "${RCFILE}"

	if test "x$2" = "xmodrc"; then
	    [ -z "$verbose" ] || log_msg_ok    "mod rc...";
	    testrun1b_modrc
	fi

	./samhain -t init -p none -l info

	if test x$? = x0; then
	    [ -z "$verbose" ] || log_msg_ok    "init...";
	else
	    [ -z "$quiet" ]   && log_msg_fail  "init...";
	    return 1
	fi

}

do_test_1b () {

    ./samhain -t check -p none -l info
    
    if test x$? = x0; then
	./samhain -j -L $LOGFILE >"${LOGFILE}.tmp" && mv "${LOGFILE}.tmp" "${LOGFILE}"
	if [ $? -ne 0 ]; then
	    [ -z "$quiet" ]   && log_msg_fail  "mv logfile...";
	    return 1
	fi
	[ -z "$verbose" ] || log_msg_ok    "check...";
    else
	[ -z "$quiet" ]   && log_msg_fail  "check...";
	return 1
    fi
    #
    tmp=`egrep "Checking.*/etc(>|\")" $LOGFILE 2>/dev/null | wc -l`
    if [ $tmp -ne 2 ]; then
	[ -z "$verbose" ] || log_msg_fail "/etc";
	return 1
    fi
    tmp=`egrep "Checking.*(>|\")" $LOGFILE 2>/dev/null | wc -l`
    if [ $tmp -ne 10 ]; then
	[ -z "$verbose" ] || log_msg_fail "checking";
	return 1
    fi
    egrep "ADDED" $LOGFILE >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	[ -z "$verbose" ] || log_msg_fail "init was incomplete";
	return 1
    fi
    #
    return 0
}

testrun1i ()
{
    log_start "RUN STANDALONE W/STEALTH W/SIGNIFY"
    SIGNIFY=`find_path signify-openbsd`
    if [ -z "$SIGNIFY" ]; then
	SIGNIFY=`find_path signify`
    fi
    if [ -z "$SIGNIFY" ]; then
	log_skip 1 $MAXTEST 'signify not found in $PATH'
	log_skip 2 $MAXTEST 'signify not found in $PATH'
	log_skip 3 $MAXTEST 'signify not found in $PATH'
	log_skip 4 $MAXTEST 'signify not found in $PATH'
    else
	eval "ls ~/.signify/samhain.pub >/dev/null 2>/dev/null"
	if [ $? -ne 0 ]; then
	    log_skip 1 $MAXTEST 'public key ~/.signify/samhain.pub not present'
	    log_skip 2 $MAXTEST 'public key ~/.signify/samhain.pub not present'
	    log_skip 3 $MAXTEST 'public key ~/.signify/samhain.pub not present'
	    log_skip 4 $MAXTEST 'public key ~/.signify/samhain.pub not present'
	else
	    #
	    # -------------  first test -------------
	    #
	    BUILDOPTS="--quiet $TRUST --enable-debug --with-signify=${SIGNIFY} --enable-micro-stealth=137 --enable-login-watch --prefix=$PW_DIR --localstatedir=$PW_DIR --with-config-file=$RCFILE  --with-log-file=$PW_DIR/.samhain_log --with-pid-file=$PW_DIR/.samhain_lock --with-data-file=$PW_DIR/.samhain_file"
	    testrun1b_internal "${BUILDOPTS}" 
	    do_test_1b
	    if [ $? -eq 0 ]; then
		log_ok   1 $MAXTEST 'signify signed config/database files'
	    else
		log_fail 1 $MAXTEST 'signify signed config/database files'
	    fi


	    #
	    # -------------  second test -------------
	    #
	    BUILDOPTS="--quiet $TRUST --enable-debug --with-signify=${SIGNIFY} --with-checksum --enable-micro-stealth=137 --enable-login-watch --prefix=$PW_DIR --localstatedir=$PW_DIR --with-config-file=$RCFILE  --with-log-file=$PW_DIR/.samhain_log --with-pid-file=$PW_DIR/.samhain_lock --with-data-file=$PW_DIR/.samhain_file"
	    testrun1b_internal "${BUILDOPTS}" 
	    do_test_1b
	    if [ $? -eq 0 ]; then
		log_ok   2 $MAXTEST 'signify signed config/database files'
	    else
		log_fail 2 $MAXTEST 'signify signed config/database files'
	    fi


	    #
	    # -------------  third test -------------
	    #
	    BUILDOPTS="--quiet $TRUST --enable-debug --with-signify=${SIGNIFY} --with-checksum --with-pubkey-checksum=62F3EAE3CD9BA8849015060750908790B6326015A20AC0DA --enable-micro-stealth=137 --enable-login-watch --prefix=$PW_DIR --localstatedir=$PW_DIR --with-config-file=$RCFILE  --with-log-file=$PW_DIR/.samhain_log --with-pid-file=$PW_DIR/.samhain_lock --with-data-file=$PW_DIR/.samhain_file"
	    testrun1b_internal "${BUILDOPTS}" 
	    do_test_1b
	    if [ $? -eq 0 ]; then
		log_ok   3 $MAXTEST 'signify signed config/database files'
	    else
		log_fail 3 $MAXTEST 'signify signed config/database files'
	    fi


	    #
	    # -------------  fourth test -------------
	    #
	    PRECONV=`find_path convert`
	    "${PRECONV}" --help | grep  ImageMagick >/dev/null 2>&1 && \
 		CONVERT="${PRECONV}"

	    if [ -z "$CONVERT" ]; then
		log_skip 2 $MAXTEST 'ImageMagick convert not found in $PATH'
	    else
		BUILDOPTS="--quiet $TRUST --enable-debug --with-signify=${SIGNIFY} --with-checksum --enable-stealth=137 --enable-login-watch --prefix=$PW_DIR --localstatedir=$PW_DIR --with-config-file=$RCFILE  --with-log-file=$PW_DIR/.samhain_log --with-pid-file=$PW_DIR/.samhain_lock --with-data-file=$PW_DIR/.samhain_file"
		testrun1b_internal "${BUILDOPTS}" "$CONVERT"
		do_test_1b
		if [ $? -eq 0 ]; then
		    log_ok   4 $MAXTEST 'signify signed config/database files'
		else
		    log_fail 4 $MAXTEST 'signify signed config/database files'
		fi
	    fi

	fi
    fi
    log_end "RUN STANDALONE W/STEALTH W/SIGNIFY"
    return 0
}

