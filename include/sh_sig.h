/* SAMHAIN file system integrity testing                                   */
/* Copyright (C) 1999 Rainer Wichmann                                      */
/*                                                                         */
/*  This program is free software; you can redistribute it                 */
/*  and/or modify                                                          */
/*  it under the terms of the GNU General Public License as                */
/*  published by                                                           */
/*  the Free Software Foundation; either version 2 of the License, or      */
/*  (at your option) any later version.                                    */
/*                                                                         */
/*  This program is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/*  GNU General Public License for more details.                           */
/*                                                                         */
/*  You should have received a copy of the GNU General Public License      */
/*  along with this program; if not, write to the Free Software            */
/*  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              */

#if defined(WITH_SIG) 

#ifndef SH_SIG_H
#define SH_SIG_H

typedef enum {
  SIG_CONF,
  SIG_DATA
} ShSigFile;

/* Top level function to verify file.
 */
SL_TICKET sh_sig_extract_signed(SL_TICKET fd);
SL_TICKET sh_sig_extract_signed_data(SL_TICKET fd);

/* this function exits if configuration file
 * and/or database cannot be verified; otherwise returns 0
 */
int sh_sig_check_signature (SL_TICKET file, ShSigFile what);

int sh_sig_msg_start(const char * line);
int sh_sig_msg_startdata(const char * line);
int sh_sig_msg_end(const char * line);
int sh_sig_data_end(const char * line);

/* log successful startup
 */
void sh_sig_log_startup (void);

#endif

/* #ifdef WITH_SIG */
#endif














